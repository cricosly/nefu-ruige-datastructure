#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct node
{
    int data[maxsize];
    int top;
}Stack;

void rustack(Stack &S,int x,int d)//除基取余，余入栈
{
    S.top = -1;
    int y;//y为商
    y = x;
    while(y!=0)
    {
        if(S.top==maxsize-1)
            break;
        S.top++;  //top先++
        S.data[S.top] = y % d; //再入栈
        y = y / d;
    }
}

void outputstack(Stack &S)//连续出栈
{
    while(S.top!=-1)
    {
        cout << S.data[S.top]; //先出栈
        S.top--;//top再--
    }
}

int main()
{
    Stack S;
    int x, d;//x为原数，d为目标转化进制
    cin >> x >> d;
    rustack(S,x,d);
    cout << x << "(10)=";
    outputstack(S);
    cout << "(" << d << ")";
    return 0;
}
