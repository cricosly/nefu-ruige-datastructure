#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct node
{
    int data[maxsize];
    int top;
}Stack;

void creatstack(Stack &S)//连续入栈
{

    S.top = -1;
    int x;
    cin >> x;
    while(x!=0)
    {
        if(S.top==maxsize-1)
            break;
        S.top++;  //top先++
        S.data[S.top] = x;//再入栈
        cin >> x;
    }
}

void outputstack(Stack &S)//连续出栈
{
    while(S.top!=-1)
    {
        cout << S.data[S.top] << ' ';//先出栈
        S.top--;//top再--
    }
}

int main()
{
    Stack S;
    creatstack(S);
    outputstack(S);
    return 0;
}
