#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct node
{
    int data;
    struct node *next;
}Qnode,*LQnode;

typedef struct
{
    LQnode front,rear;
}Queue;//头尾指针封装

void chushiqueue(Queue &Q)//初始化队列
{
    Q.front = Q.rear = new Qnode;
    if(!Q.front)
    {
        cout << "error!";
        return;
    }
    Q.front->next = NULL;
}

void cinqueue(Queue &Q,int x)//每个元素单独入队列
{
    Qnode *p;
    p = new Qnode;
    p->data = x;
    p->next = NULL;
    Q.rear->next = p;//此时Q.front=Q.rear，所以Q.front->next=p
    Q.rear = p;//此时Q.front!=Q.rear,之后Q.front不会移动了，只指向头元素，Q.rear在尾元素结点上，Q.rear->next = NULL
}

void outputqueue(Queue &Q)//输出队列
{
    Qnode *p;
    while(Q.front!=Q.rear)
    {
        p = Q.front->next;//p为完成出队列后的首元素结点
        cout << p->data<<' ';
        Q.front->next = p->next;//Q.front指向下一个元素
        if(Q.rear==p)//Q.rear在尾元素结点上
        {
            Q.rear = Q.front;//只剩一个元素，Q.rear = Q.front
        }
        free(p);
    }
}

int main()
{
    Queue Q;
    int x;
    chushiqueue(Q);
    cin >> x;
    while(x!=0)
    {
        cinqueue(Q, x);
        cin >> x;
    }
    outputqueue(Q);
    return 0;
}
