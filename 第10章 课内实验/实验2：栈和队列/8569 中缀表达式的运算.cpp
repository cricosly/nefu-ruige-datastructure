#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct
{
    int data[maxsize];
    int top;
}Stack1;
typedef struct
{
    char data[maxsize];
    int top;
}Stack2;
/*
void push(Stack &S,char x)//单元素入栈
{
        if(S.top==maxsize-1)
            return;
        S.top++;  //top先++
        S.data[S.top] = x; //再入栈
}
void outputstack(Stack &S,char &y)//单元素出栈
{
    if(S.top==-1)
            return;
    else
    {
        y = S.data[S.top]; //先出栈
        S.top--;//top再--
    }
}*/

int yunsuan(int x,char ch,int y)//运算数据
{
    switch(ch)
    {
        case '+':return x+y;
        case '-':return x-y;
        case '*':return x*y;
        case '/':return x/y;
        default:return 0;        
    }
}


int lv(char ch) {
    int level = 0; // 优先级
    switch(ch) {
        case '#':
            level = 0;
            break;
        case '(':
            level = 1;
            break;
        case '+':
        case '-':
            level = 2;
            break;
        case '*':
        case '/':
            level = 3;
            break;
        default:
            break;
    }
    return level;
}

int express(char *str)//中缀表达式运算
{
    int i, z;
    int x, y;//运算时传递运算数
    char ch;//运算时传递运算符
    i = 0;//表达式的下标
    Stack1  shu;//数栈
    Stack2  fu;//符号栈
    shu.top = -1;
    fu.top = -1;
    fu.data[fu.top+1]='#';//让符号栈表头为#
    fu.top++;//fu.top=0
    while(str[i]!='#')//表达式以#结尾
    {
        if(str[i]>='0'&&str[i]<='9')//数字入栈
        {
            shu.top++;
            shu.data[shu.top] = str[i] - '0';
            i++;
            continue;//立即进行下一次的循环
        }
        else//符号
        {
            if(fu.data[fu.top]=='('||fu.data[fu.top]=='#'||(str[i]=='('))//栈顶为#，栈顶为（和遇到（，遇到的符号直接入符号栈
            {
                fu.top++;
                fu.data[fu.top] = str[i];
                i++;
            }
            else if(str[i]==')')//遇到右括号，一直处理到左括号
            {
            	while(fu.data[fu.top]!='(')//一直处理到左括号
            	{
            	    ch=fu.data[fu.top];fu.top--;
            	    y=shu.data[shu.top];shu.top--;
                    x=shu.data[shu.top];shu.top--;
                    z=yunsuan(x,ch,y);//运算
                    shu.data[shu.top+1]=z;shu.top++;//运算结果入数字栈
                }
               	fu.top--;//删除左括号
               	i++;//跳过右括号，右括号不入栈
                continue;
            }
            else if(lv(str[i])>lv(fu.data[fu.top]))//遇到符号 > 栈顶符号，入栈
            {
                fu.top++;
                fu.data[fu.top] = str[i];
                i++;
            }
            else if(lv(str[i])<=lv(fu.data[fu.top]))//遇到符号 <= 栈顶符号，运算
            {
                ch=fu.data[fu.top];fu.top--;
            	y=shu.data[shu.top];shu.top--;
                x=shu.data[shu.top];shu.top--;
                z=yunsuan(x,ch,y);//运算
                shu.data[shu.top+1]=z;shu.top++;//运算结果入数字栈
            }
        }
    }
    while(fu.data[fu.top]!='#')
    {
        ch=fu.data[fu.top];fu.top--;
        y=shu.data[shu.top];shu.top--;
        x=shu.data[shu.top];shu.top--;
        z=yunsuan(x,ch,y);//运算
        shu.data[shu.top+1]=z;shu.top++;//运算结果入数字栈
    }
    return shu.data[shu.top];
}

int main()
{
    char str[maxsize];
    int a;//存运算结果
    gets(str);
    a = express(str);
    cout << a;
    return 0;
}

/*老师算法
#include  <stdio.h>
typedef struct
{
    int data[100];
    int top;
}Stack1;
typedef struct
{
    char data[100];
    int top;
}Stack2;
int yunsuan(int x,char th,int y)
{
    switch(th)
    {
        case '+': return x+y;
        case '-': return x-y;
        case '*': return x*y;
        case '/': return x/y;
        default: return 0;
    }
}
//算法3.22　表达式求值
int EvaluateExpression(char *str)
{
    char theta;
    int a,b,c,i;
    Stack1 shu;
    Stack2 fu;
    shu.top=-1;
    fu.top=-1;
    fu.data[fu.top+1]='#';
    fu.top++;
    i=0;
    while(str[i]!='#')
    {
	if (str[i]>='0'&&str[i]<='9')//数字入栈
	{
	    shu.data[shu.top+1]=str[i]-'0';
	    shu.top++;
            i++;
            continue;
	}
	else
	{
	    if(fu.data[fu.top]=='('||fu.data[fu.top]=='#'||(str[i]=='('))
            {
            	fu.data[fu.top+1]=str[i];
	       	fu.top++;
            	i++;continue;
            }
	    else if(str[i]==')')
            {
            	while(fu.data[fu.top]!='(')
            	{
            	    theta=fu.data[fu.top];fu.top--;
            	    a=shu.data[shu.top];shu.top--;
                    b=shu.data[shu.top];shu.top--;
                    c=yunsuan(b,theta,a);
                    shu.data[shu.top+1]=c;shu.top++;
                }
               	fu.top--;
               	i++;continue;
            }
        else  if(str[i]=='+'||str[i]=='-')
            {
		        theta=fu.data[fu.top];fu.top--;
            	a=shu.data[shu.top];shu.top--;
            	b=shu.data[shu.top];shu.top--;
            	c=yunsuan(b,theta,a);
            	shu.data[shu.top+1]=c;shu.top++;
	        }
	    else if((str[i]=='*'||str[i]=='/')&&(fu.data[fu.top]=='+'||fu.data[fu.top]=='-'))//遇到符号优先级高于栈顶的符号
            {
	        fu.data[fu.top+1]=str[i];
	        fu.top++;
            	i++;continue;
	        }
	    else if((str[i]=='*'||str[i]=='/')&&(fu.data[fu.top]=='*'||fu.data[fu.top]=='/'))//遇到符号与栈顶的符号同级
            {
            	theta=fu.data[fu.top];fu.top--;
            	a=shu.data[shu.top];shu.top--;
            	b=shu.data[shu.top];shu.top--;
            	c=yunsuan(b,theta,a);
            	shu.data[shu.top+1]=c;shu.top++;
            }
	}
    }
    while(fu.data[fu.top]!='#')
    {
        theta=fu.data[fu.top];fu.top--;
        a=shu.data[shu.top];shu.top--;
        b=shu.data[shu.top];shu.top--;
        c=yunsuan(b,theta,a);
        shu.data[shu.top+1]=c;shu.top++;
    }
    return shu.data[shu.top];
}
int main()
{
    char str[100];
    int a;
    gets(str);
    a=EvaluateExpression(str);
    printf("%d\n",a);
    return 0;
}
*/