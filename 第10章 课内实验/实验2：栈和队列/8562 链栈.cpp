#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct node
{
    int data;
    struct node *next;
}Stacknode,*LStack;

void creatstack(LStack &top)//连续入栈，头插法
{
    top = NULL;//栈顶为空
    Stacknode *p;
    int x;
    cin >> x;
    while(x!=0)
    {
        p = new Stacknode;
        p->data = x;
        p->next = top; //头插法，入栈
        top = p;//top移动
        cin >> x;
    }
}

void outputstack(LStack &top)//连续出栈
{
    Stacknode *p;
    while(top!=NULL)
    {
        p = top;
        cout << top->data << ' ';//出栈
        top=top->next;//top移动
        free(p);
    }
}

int main()
{
    LStack S;
    creatstack(S);
    outputstack(S);
    return 0;
}
