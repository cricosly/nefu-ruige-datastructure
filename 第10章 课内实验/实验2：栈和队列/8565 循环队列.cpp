#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct node
{
    int data[maxsize];
    int front, rear;
}Queue,*LQnode;

void chushiqueue(Queue &Q)//初始化队列
{
    Q.front = Q.rear = 0;
}

void cinqueue(Queue &Q,int x)//每个元素单独入队列
{
    if((Q.rear+1)%maxsize==Q.front)
    {
        cout << "队列满";
        return;
    }
    Q.data[Q.rear] = x;
    Q.rear = (Q.rear + 1) % maxsize;
}

void outputqueue(Queue &Q)//输出队列
{
    while(Q.front!=Q.rear)
    {
        cout << Q.data[Q.front]<<' ';
        Q.front = (Q.front + 1) % maxsize;
    }
}

int main()
{
    Queue Q;
    int x;
    chushiqueue(Q);
    cin >> x;
    while(x!=0)
    {
        cinqueue(Q, x);
        cin >> x;
    }
    outputqueue(Q);
    return 0;
}
