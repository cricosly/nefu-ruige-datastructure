#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;

typedef struct{
    int r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

void ShellInsert(SqList &L, int dk)
{
    for(int i=dk+1; i<=L.len; i++)
        if(L.r[i] < L.r[i-dk])
        {
            L.r[0] = L.r[i];
            int j;
            for(j=i-dk; j>0&&L.r[0]<L.r[j]; j-=dk)
                L.r[j + dk] = L.r[j];
            L.r[j+dk] = L.r[0];
        }
}

void ShellSort(SqList &L, int *dt, int t)//希尔排序
{
    for(int k=0; k<t; k++)
        ShellInsert(L, dt[k]);
}

int main()
{
    SqList L;
    int x, len, dt[3]={5, 3, 1};
    len = 0;
    cin >> x;
    while(x)
    {
        len++;
        L.r[len] = x;
        cin >> x;
    }
    L.len = len;
    ShellSort(L, dt, 3);
    for(int i=1; i<=len; i++)
        cout<<L.r[i]<<" ";
    return 0;
}