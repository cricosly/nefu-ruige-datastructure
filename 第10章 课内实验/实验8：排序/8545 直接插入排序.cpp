#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;
typedef struct{
    int r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

void InsertSort(SqList &L)
{
    for(int i=2; i<=L.len; i++)
        if(L.r[i] < L.r[i-1])
        {
            L.r[0] = L.r[i];
            L.r[i] = L.r[i-1];
            int j;
            for(j=i-2; L.r[0]<L.r[j]; j--)
                L.r[j+1] = L.r[j];
            L.r[j+1] = L.r[0];
        }
}

int main()
{
    SqList L;
    int x, len;
    len = 0;
    cin >> x;
    while(x)
    {
        len++;
        L.r[len] = x;
        cin >> x;
    }
    L.len = len;
    InsertSort(L);
    for(int i=1; i<=len; i++)
        cout<<L.r[i]<<" ";
    return 0;
}