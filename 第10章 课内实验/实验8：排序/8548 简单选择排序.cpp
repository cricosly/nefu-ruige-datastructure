#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;

typedef struct{
    int r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

void SelectSort(SqList &L)//选择排序
{
    for(int i=1; i<L.len; i++)
    {
        int k = i;
        for(int j=i+1; j<=L.len; j++)
            if(L.r[j] < L.r[k]) k = j;
        if(k != i)
        {
            swap(L.r[i], L.r[k]);
        }
    }
}
int main()
{
    SqList L;
    int x, len;
    len = 0;
    cin >> x;
    while(x)
    {
        len++;
        L.r[len] = x;
        cin >> x;
    }
    L.len = len;
    SelectSort(L);
    for(int i=1; i<=len; i++)
        cout<<L.r[i]<<" ";
    return 0;
}