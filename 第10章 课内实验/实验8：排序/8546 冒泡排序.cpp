#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;
typedef struct{
    int r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

void BubbleSort(SqList &L)//冒泡排序
{
    int m = L.len - 1, i, flag;
    cout << "leng-1: " << m << endl;
    flag = 1;
    while(m>0 && flag)
    {
        flag = 0;
        for(i=1; i<=m; i++)
            if(L.r[i] > L.r[i+1])
            {
                flag = true;
                swap(L.r[i], L.r[i+1]);
            }
        m--;
    }
}
/*
	for(k=0;k<n-1;k++)
	{
		for(j=0;j<n-1-k;j++)
		{
			if(a[j]>a[j+1])
			{
				temp=a[j];
				a[j]=a[j+1];
				a[j+1]=temp;
			}
		}
	}
*/
int main()
{
    SqList L;
    int x, len=0;
    len++;
    while(cin>>x && x)
    {
        L.r[len] = x;
        len++;
    }
    L.len = len-1;
    BubbleSort(L);
    for(int i=1; i<len; i++)
        cout<<L.r[i]<<" ";
    return 0;
}