#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;

typedef struct{
    int r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

void HeapAdjust(SqList &L, int s, int m)//假设r[s+1..m]已经是堆，将r[s..m]调整为以r[s]为根的大根堆
{
    int p;
    p = L.r[s];
    for(int i=2*s; i<=m; i*=2)
    {
        if(i<m && L.r[i]<L.r[i+1]) i++;
        if(p >= L.r[i]) break;
        L.r[s] = L.r[i];
        s = i;
    }
    L.r[s] = p;
}

void CreatHeap(SqList &L) // 把无序序列L.r[1..n]建成大根堆
{
    int n, i;
    n = L.len;
    for(i=n/2; i>=1; i--)
        HeapAdjust(L, i, n);
}

void HeapSort(SqList &L)//堆排序
{
    CreatHeap(L);
    for(int i=L.len; i>1; i--)
    {
        int x;
        x = L.r[1];
        L.r[1] = L.r[i];
        L.r[i] = x;
        HeapAdjust(L, 1, i-1);
    }
}

int main()
{
    SqList L;
    int x, len, i;
    len = 0;
    cin >> x;
    while(x)
    {
        len++;
        L.r[len] = x;
        cin >> x;
    }
    L.len = len;
    HeapSort(L);
    for(i=1; i<=len; i++)
        cout<<L.r[i]<<" ";
    return 0;
}
