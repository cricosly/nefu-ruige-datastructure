#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;
typedef struct{
    int r[maxsize+1]; // r[0]闲置或用作哨兵单元
    int len;
}SqList;

int Partition(SqList &L, int low, int high)//一趟快速排序
{
    L.r[0] = L.r[low];
    int pivotkey;
    pivotkey = L.r[low];
    while(low<high)
    {
        while(low<high && L.r[high]>=pivotkey)
            high--;
        L.r[low] = L.r[high];
        while(low<high && L.r[low]<=pivotkey)
            low++;
        L.r[high] = L.r[low];
    }
    L.r[low] = L.r[0];
    return low;//返回支点记录所在位置
}

void QuickSort(SqList &L, int low, int high)//快速排序r[low...high]
{
    if(low<high)
    {
        int pivotloc = Partition(L, low, high);//r[]一分为二
        QuickSort(L, low, pivotloc-1);//左子区间快速排序
        QuickSort(L, pivotloc+1, high);//右子区间快速排序
    }
}

int main()
{
    SqList L;
    int x, len;
    len=0;
    cin >> x;
    while(x)
    {
        len++;
        L.r[len] = x;
        cin >> x;
    }
    L.len = len;
    QuickSort(L, 1, L.len);
    for(int i=1; i<=len; i++)
        cout<<L.r[i]<<" ";
    return 0;
}