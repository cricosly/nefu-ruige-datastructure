#include <iostream>
using namespace std;
typedef struct node
{
    int coef,exp;//coef系数，exp指数
    struct node *next;
}Node,*LNode;

void output(LNode &H)//输出链表
{
    LNode p;
    p=H->next;
    while(p!=NULL)
    {
        cout << p->coef << "*x^" << p->exp << ' ';
        //printf("%d  ",p->data);
        p=p->next;
    }
    //cout << endl;
}

void insertnode(LNode &H,int x,int y)//元素以 指数单调不减 插入链表
{
    Node *p, *q, *r;//r前驱，p工作指针,q创结点
    r = H;
    p = H->next;
    while(p!=NULL&&p->exp<y)
    {
        r = p;
        p = p->next;
    }
    q = new Node;
    q->coef = x;
    q->exp = y;
    q->next=p;//插到工作指针之前
    r->next=q;
}

void cinnode(LNode &H,int n)//输入元素
{
    H = new Node;
    if(H==NULL)
    {
        cout << "无内存空间可分配" << endl;
        return;
    }
    H->next = NULL;
    int x, y, i;
    char ch;
    for (i = 0; i < n; i++)
    {
        cin >> x;
        cin >> ch;
        cin >> y;
        insertnode(H, x, y); //将元素插入链表
    }
}

void add(LNode &HA,LNode &HB)//递增不重复连接链表A,B.尾插连接
{
    int sum;
    Node *pre, *pa, *pb;
    pre = HA;
    pa = HA->next;
    pb = HB->next;
    while(pa!=NULL&&pb!=NULL)
    {
        if(pa->exp<pb->exp)
        {
            pre->next = pa;
            pre=pa;
            pa = pa->next;
        }
        else if(pa->exp==pb->exp)
        {
            sum = pa->coef + pb->coef;
            if(sum!=0)
            {
                pa->coef = sum;
                pre->next = pa;
                pre = pa;
                pa = pa->next;
                pb = pb->next;
            }
            else
            {
                pa = pa->next;
                pb = pb->next;
            }
            
        }
        else
        {
            pre->next = pb;
            pre=pb;
            pb = pb->next;
        }
    }
    while(pa!=NULL)
    {
        pre->next = pa;
        pre = pa;
        pa = pa->next;
    }
    while(pb!=NULL)
    {
        pre->next = pb;
        pre = pb;
        pb = pb->next;
    }
    pre->next = NULL;
}

int main()
{
    int n;
    LNode HA,HB;
    cin >> n;
    cinnode(HA,n);
    cin >> n;
    cinnode(HB,n);
    add(HA, HB);
    output(HA);
    return 0;
}
