#include <iostream>
using namespace std;
typedef struct node
{
    int data;
    struct node *next;
}Node,*LNode;

void creatnode(LNode &H)//创链表,尾插法
{
    H = new Node;
    if(H==NULL)
    {
        cout << "无内存空间可分配" << endl;
        return;
    }
    H->next = NULL;
    Node *q, *r;//q创结点，r尾结点前驱串节点,尾插法
    int x;
    r = H;
    cin >> x;
    while(x!=0)
    {
        q = new Node;
        q->data = x;
        q->next = r->next;//r尾结点
        r->next = q;
        r = q;
        cin >> x;
    }
}

void outputnode(LNode &H)//输出链表
{
    Node *p;
    p=H->next;
    while(p!=NULL)
    {
        cout << p->data << ' ';
        //printf("%d  ",p->data);
        p=p->next;
    }
}

void reversenode(LNode &H)//头插法逆置
{
    Node *p, *q;
    p = H->next;//p记录首元结点,p作为头插法待插入元素
    H->next = NULL;
    while(p!=NULL)
    {
        q = p->next;//q记录待插入元素的下一个结点
        p->next = H->next;//H->next = NULL
        H->next = p;
        p = q;//p移到原链表下一个下一个结点
    }
}
int main()
{
    LNode H;
    creatnode(H);
    reversenode(H);
    outputnode(H);
    return 0;
}
