#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;
typedef struct node
{
    int data;
    struct node *next;
}Node,*LNode;

void creatnode(LNode &H)//创链表,尾插法
{
    H = new Node;
    if(H==NULL)
    {
        cout << "无内存空间可分配" << endl;
        return;
    }
    H->next = NULL;
    Node *q, *r;//q创节点，r前驱串节点,尾插法
    int x;
    r = H;
    cin >> x;
    while(x!=0)
    {
        q = new Node;
        q->data = x;
        q->next = r->next;
        r->next = q;
        r = q;
        cin >> x;
    }
} 

void outputnode(LNode &H)//输出链表
{
    Node *p;
    p=H->next;
    while(p!=NULL)
    {
        cout << p->data << ' ';
        //printf("%d  ",p->data);
        p=p->next;
    }
}

void splitnode(LNode &HA,LNode &HB)//单链表一分为二，A奇数 B偶数
{
    LNode pa, paa, pbb;
    HB = new Node;
    HB->next = NULL;
    paa = HA;
    pa = HA->next;
    pbb = HB;
    while(pa!=NULL)
    {
        if(int(fabs((float)pa->data))%2==1)//奇数
        {
            paa->next = pa;
            paa = pa;
            pa = pa->next;
        }
        else
        {
            pbb->next = pa;
            pbb = pa;
            pa = pa->next;
        }
    }
    paa->next = NULL;
    pbb->next = NULL;
}


int main()
{
    LNode HA,HB;
    creatnode(HA);
    splitnode(HA, HB);
    outputnode(HA);
    cout << endl;
    outputnode(HB);
    return 0;
}
