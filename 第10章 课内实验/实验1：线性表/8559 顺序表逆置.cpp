#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct
{
    int data[maxsize];
    int length;
}Seqlist;

void creatlist(Seqlist &L)//创顺序表
{ 
  int n,i;
  cin >> n;//输入n个数
  L.length = n;
  for (i = 0; i < n; i++)
  {
      cin >> L.data[i];
  }
}

void outputlist(Seqlist &L)//输出顺序表
{
    int i, n;
    n = L.length;
    for (i = 0; i < n; i++)
    {
        cout << L.data[i] << ' ';
    }
}

void reverselist(Seqlist &L)//逆置顺序表
{
    int i, j, temp;
    j = L.length-1;
    for (i = 0; i < j; i++, j--)
    {
        temp = L.data[i];
        L.data[i] = L.data[j];
        L.data[j] = temp;
    }
}

int main()
{
    Seqlist L;
    creatlist(L);
    outputlist(L);
    cout << endl;
    reverselist(L);
    outputlist(L);
    return 0;
}
