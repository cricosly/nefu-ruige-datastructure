#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct node
{
    int data;
    struct node *next;
}Node,*LNode;

void creatnode(LNode &H)//创链表,尾插法
{
    H = new Node;
    if(H==NULL)
    {
        cout << "无内存空间可分配" << endl;
        return;
    }
    H->next = NULL;
    Node *q, *r;//q创节点，r尾结点前驱串节点,尾插法
    int x;
    r = H;
    cin >> x;
    while(x!=0)
    {
        q = new Node;
        q->data = x;
        q->next = r->next;//r->next=NULL
        r->next = q;
        r = q;//H的位置没变
        cin >> x;
    }
}

void outputnode(LNode &H)//输出链表
{
    Node *p;
    p=H->next;
    while(p!=NULL)
    {
        cout << p->data << ' ';
        //printf("%d  ",p->data);
        p=p->next;
    }
}

int main()
{
    LNode H;
    creatnode(H);
    outputnode(H);
    return 0;
}
