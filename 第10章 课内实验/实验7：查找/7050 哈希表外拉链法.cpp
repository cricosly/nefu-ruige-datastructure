#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;

typedef struct node
{
    int data;
    struct node *next;
}LNode, *LinkList;

typedef struct HashList//头结点
{
    int len;
    LinkList List;//存头结点数组
}Hashlist;

void HashInsert(Hashlist HL,int m, int k)
{
    int p;
    p = m;
    int h;
    h = k % p;
    if(HL.List[h].data==0)
        HL.List[h].data = k;
    else
    {
        LNode *p = new LNode;
        p->data = HL.List[h].data;
        p->next = HL.List[h].next;
        HL.List[h].data = k;
        HL.List[h].next = p;//头插法插入链表
    }
    
}
void creatHash(HashList HL,int m)
{
    int k;//关键字
    cin>>k;
    while(k)//输入0停止
    {
        HashInsert(HL,m,k);
        cin>>k;
    }

}
int main()
{
    HashList HL;
    int i;
    HL.len = 13;//13个散列地址
    HL.List = new LNode[13];//申请HL.List[13]的地址
    for (i = 0; i < 13; i++)//初始化头结点
    {
        HL.List[i].data = 0;
        HL.List[i].next = NULL;
    }
    creatHash(HL,HL.len);
    for(int i=0; i<13; i++)
    {
        cout<<i<<":";
        if(HL.List[i].data!=0)
            cout << HL.List[i].data << " ";
        LinkList p;
        p = HL.List[i].next;
        while(p)
        {
            cout<<p->data<<" ";
            p = p->next;
        }
        cout<<endl;
    }
    return 0;
}