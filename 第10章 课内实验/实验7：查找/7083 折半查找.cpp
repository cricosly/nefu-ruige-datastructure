#include <iostream>
#include <stack>
#include <queue>
#include <algorithm>
using namespace std;
int a[100];
int main()
{
    int i=0, x, sum=0;
    while(cin>>x && x)
    {
        i++;//下标从1开始
        a[i] = x;
    }//共i个数据
    sort(a+1, a+i+1);//a[1]~a[i]从小到大排序
    cin>>x;//输入待查找数
    int L, R;
    L = 1, R = i; // 左侧下表与右侧下表，共i个数据
    while(L<=R)
    {
        sum++;//比较次数
        int mid;
        mid = (L + R) / 2; // 中间下标
        if(a[mid]<x)
            L = mid + 1;
        else if(a[mid]>x)
            R = mid - 1;
        else
        {
            cout<<mid<<" "<<sum<<endl;
            return 0;
        }
    }
    cout<<0<<" "<<sum<<endl;
    return 0;
}