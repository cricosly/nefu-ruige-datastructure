
#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;
typedef struct
{
    int key;//关键字
    int count;//比较次数
}Hash[maxsize];
void creatHash(Hash H, int m, int mod);
void cinHash(Hash H, int m, int mod, int k);
int maxsu(int m); // 小于等于m的最大素数
int prime(int n); // 判断素数

int prime(int n)//判断素数
{
    
    int i;
    for(i=2;i<n;i++)
    {
        if(n%i==0)
        {
            return 0;
        }
    }
    return 1;
}

int maxsu(int m)//小于等于m的最大素数
{
    int i, su;
    i = 2;
    su = 2;
    while(i<=m)
    {
        if(prime(i)==1)
        {
            su = i;
        }
        i++;
    }
    return su;
}
void cinHash(Hash H, int m,int mod, int k)
{
    int h;//散列地址
    int i, hi;
    h = k % mod; // 哈希函数H(k)=k%13
    if(H[h].key == 0)//不冲突，直接入表
    {
        H[h].key = k;//入散列表
        H[h].count = 1;//比较次数
    } 
    else
    {
        i = 0;
        do{
            h++;
            hi = h % m;//线性探测法解决冲突
            i++;//比较次数
        } while (H[hi].key != 0);
        H[hi].key = k;
        H[hi].count = i;
    }
}

void creatHash(Hash H,int m,int mod)
{
    int i, k;
    for (i = 0; i < m; i++)//初始化哈希表
    {
        H[i].key = 0;
        H[i].count = 0;
    }
    cin >> k;
    while (k != 0)
    {
        cinHash(H, m, mod, k);
        cin >> k;
    } 
}

int main()
{
    int i, m , mod;
    Hash H;
    cin >> m;
    mod = maxsu(m);
    creatHash(H, m, mod);
    for (i = 0; i < m; i++)
        cout << i << "  ";
    cout << endl;
    for (i = 0; i < m; i++)
        cout << H[i].key << "  ";
    return 0;
}
