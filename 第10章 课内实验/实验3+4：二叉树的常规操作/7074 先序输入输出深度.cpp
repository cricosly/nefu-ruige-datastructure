#include <iostream>
#include <queue>
using namespace std;
#define maxsize 1000
typedef struct Binode
{
    char data;
    struct Binode *lchild, *rchild;
}BiNode,*Bitree;

Bitree creatbitree()//递归法先序建立二叉树
{
    Bitree T;
    char ch;
    //cin >> ch;
    scanf("%c", &ch);
    if(ch=='@')
        T = NULL;
    else
    {
        T = new BiNode;
        T->data = ch;
        T->lchild = creatbitree();
        T->rchild = creatbitree();
    }
    return T;
}

int depth(Bitree T)//二叉树深度
{
    int m = 0, n = 0;
    if(T==NULL)//空树
        return 0;
    else
    {
        m=depth(T->lchild);//左孩子深度
        n=depth(T->rchild);//右孩子深度
        if(m>n)
            return m + 1;
        else
            return n + 1;
    }
}

int main()
{
    Bitree T;
    int d;
    T=creatbitree();
    d=depth(T);
    cout << d;
    return 0;
}
