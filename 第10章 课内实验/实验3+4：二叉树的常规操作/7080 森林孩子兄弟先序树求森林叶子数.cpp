#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct CSnode
{
    char data;
    struct CSnode *lchild, *rchild;
}BiNode,*Bitree;

Bitree creatbitree()//递归法先序遍历建立二叉树
{
    Bitree T;
    char ch;
    //cin >> ch;
    scanf("%c", &ch);
    if(ch=='@')
        T = NULL;
    else
    {
        T = new BiNode;
        T->data = ch;
        T->lchild = creatbitree();
        T->rchild = creatbitree();
    }
    return T;
}

int leafsum(Bitree T)//求二叉树对应的森林的叶子数目（森林孩子兄弟表示法二叉树中没有左孩子的结点在森林中为叶子结点）
{
    if(T==NULL)
        return 0;
    else
    {
        if(T->lchild==NULL)
            return 1+leafsum(T->rchild);//对应森林的叶子结点
        else
            return leafsum(T->lchild) + leafsum(T->rchild);//左子树叶子数+右子树叶子数
    }
}

/*
正常求二叉树叶子结点
int sum=0;
int leafsum(Bitree T)
{
    if(T)
    {
        if(T->lchild==NULL&&T->rchild==NULL)
            sum++;
        else
        {
            leafsum(T->lchild);
            leafsum(T->rchild);
        }
    }
    return sum;
}*/

int main()
{
    int sum;
    Bitree T;
    T=creatbitree();
    sum = leafsum(T);
    cout << sum;//森林
    return 0;
}
