#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct Binode
{
    char data;
    struct Binode *lchild, *rchild;
}BiNode,*Bitree;

Bitree creatbitree()//递归法先序建立二叉树
{
    Bitree T;
    char ch;
    //cin >> ch;
    scanf("%c", &ch);
    if(ch=='@')
        T = NULL;
    else
    {
        T = new BiNode;
        T->data = ch;
        T->lchild = creatbitree();
        T->rchild = creatbitree();
    }
    return T;
}

void output(Bitree T)//递归中序输出树
{
    if(T)
    {
        output(T->lchild);//先左孩子
        cout << T->data;//中序
        output(T->rchild);//后右孩子
    }
}

int main()
{
    Bitree T;
    T=creatbitree();
    output(T);
    return 0;
}
