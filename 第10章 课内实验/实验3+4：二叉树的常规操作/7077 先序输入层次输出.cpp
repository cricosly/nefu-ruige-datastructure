#include <iostream>
#include <queue>
using namespace std;
#define maxsize 1000
typedef struct Binode
{
    char data;
    struct Binode *lchild, *rchild;
}BiNode,*Bitree;

Bitree creatbitree()//递归法先序建立二叉树
{
    Bitree T;
    char ch;
    //cin >> ch;
    scanf("%c", &ch);
    if(ch=='@')
        T = NULL;
    else
    {
        T = new BiNode;
        T->data = ch;
        T->lchild = creatbitree();
        T->rchild = creatbitree();
    }
    return T;
}

void output(Bitree T)//利用队列层次遍历输出树
{
    if(T==NULL)
        return;
    queue<Bitree> q;
    Bitree p;//end存当层最右边位置，nextend存下一层最右边位置
    q.push(T);//根入队
    while(!q.empty())
    {
        p = q.front();//p==队首
        q.pop();//队首出队
        cout << p->data;
        if(p->lchild!=NULL)//左孩子入队
        {
            q.push(p->lchild);
        }
        if(p->rchild!=NULL)//右孩子入队
        {
            q.push(p->rchild);
        }
    }
}


int main()
{
    Bitree T;
    T=creatbitree();
    output(T);
    return 0;
}

