#include <iostream>
#include <queue>
using namespace std;
#define MVNum 1000//最大顶点数
int visited[MVNum]={0};//判别顶点是否被访问过
typedef struct ArcNode//边结点
{
    int adjvex; //该边所指向的顶点的位置
    struct ArcNode *nextarc; //指向下一条边的指针
    //OtheInfo info;//边相关的信息（如权值）
}ArcNode;

typedef struct VNode//顶点信息
{ 
    int data;
    ArcNode *firstarc; //指向第一条依附该顶点的边的指针
}VNode, AdjList[MVNum];//AdjList表示邻接表类型为VNode型数组

typedef struct//邻接表
{
    AdjList adjlist;
    int vexnum, arcnum; //图的当前顶点数和边数
}ALG;//邻接表

int LocateVex(ALG G, int v)//邻接表G中v的位置
{
    for(int i=1; i<=G.vexnum; i++)
    {
        if(G.adjlist[i].data == v)
           return i;
    }
    return -1;
}

void CreateDG(ALG &G)//邻接表表示法，创建 有向图G
{
    int i, j, k, temp;
    cin >> G.vexnum >> G.arcnum;//输入图总顶点数和总边数
    for (i = 1; i <= G.vexnum; i++) // 输入各点，构建 邻接表头结点表,从1开始编号
    {
        cin >> temp;
        G.adjlist[i].data = temp;
        G.adjlist[i].firstarc = NULL; //指向第一条依附该顶点的边的指针,初始化为NULL
    }
    for (k = 0; k < G.arcnum; k++) // 输入各边，构建邻接表
    {
        int v1, v2;
        //char ch;
        cin >> v1 >> v2; // 输入一条边的两个顶点
        // scanf("%d%c%d",&v1,&ch,&v2);
        i = LocateVex(G, v1);//确定v1和v2在G中位置i和j，即顶点在G.adjlist中的序号
        j = LocateVex(G, v2);
        ArcNode *p;
        p=new ArcNode;//生成一个新的边结点
        p->adjvex = j;//该边所指向的顶点的位置为j，即该边的邻接点序号为j
        p->nextarc = G.adjlist[i].firstarc;
        G.adjlist[i].firstarc = p;//头插法，把新的边结点插入插入顶点Vi的 邻接边表 的头部
    }
}

void coutDU(ALG G)//输出入度、出度、度
{
    int indu[MVNum], outdu[MVNum], i;
    ArcNode *p;
    for (i = 1; i <= G.vexnum; i++)
    {
        indu[i] = 0;//初始化入度
        outdu[i] = 0;//初始化出度
    }
    for (i = 1; i <= G.vexnum; i++)
    {
        p = G.adjlist[i].firstarc;//p移到表头结点的第一个邻接边节点
        while(p!=NULL)
        {
           outdu[i]++;//表头结点出度+1
           indu[p->adjvex]++;//p指向的顶点入度+1
           p = p->nextarc;//p移到下一个边节点
        }
    }
    for (i = 1; i <= G.vexnum; i++)
    {
        cout << i << ':' << indu[i] << ' ' << outdu[i] << ' ' << indu[i] + outdu[i] << endl;
    }
}

int main()
{
    ALG G;
    CreateDG(G);
    coutDU(G);////输出入度、出度、度
    return 0;
}