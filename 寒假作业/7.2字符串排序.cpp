/*
输入n个字符串，将它们按字母由小到大的顺序排列并输出。编写三个函数实现，input 用于输出n个字符串，sortstr用于排序n个字符串，output 用于输出n个字符串。

输入
第一行 n
第二行到第n+1行，每行一个字符串

输出
排序后的字符串

样例输入：
3
YTU
ACM
COM

样例输出
ACM
COM
YTU
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<iostream>
using namespace std;
void input(int n,char a[][80])
{
    int i;
    for (i = 0; i < n; i++)
    {
        scanf("%s", *(a + i));
        //gets(*(a + i));
	}
}
void sortstr(int n,char a[][80])
{
    int i, j;
    for(i=0;i<n-1;i++)
	for(j=0;j<n-1-i;j++)
	{
		if(strcmp(*(a+j),*(a+j+1))>0)
		{
			strcpy(*(a+n),*(a+j));
            strcpy(*(a + j), *(a + j + 1));
            strcpy(*(a + j + 1), *(a + n));
		}
	}

}
void output(int n,char a[][80])
{
    int i;
    for (i = 0; i < n; i++)
    printf("%s\n", *(a + i));
    free(a);
}
int main()
{
    char (*a)[80];
    int n;
    cin >> n;
    a = (char(*)[80])calloc((n + 1) * 80, sizeof(char));
    getchar();
    input(n, a);
    sortstr(n, a);
    output(n, a);
    return 0;
}