#include <iostream>
using namespace std;
typedef struct Binode
{
    char data;
    struct Binode *lchild, *rchild;
}BiNode,*Bitree;
Bitree creatbitree()//递归法先序建立二叉树
{
    Bitree T;
    char ch;
    //cin >> ch;
    scanf("%c", &ch);
    if(ch=='*')
        T = NULL;
    else
    {
        T = new BiNode;
        T->data = ch;
        T->lchild = creatbitree();
        T->rchild = creatbitree();
    }
    return T;
}

int prtfa(Bitree T, char x)
{
    if(!T)//空结点
        return 0;
    if(T->data == x)//当前就是查找的结点
        return 1;
    if(prtfa(T->lchild, x) || prtfa(T->rchild, x))//当前结点的左孩子或右孩子是查找结点
    {
        cout << T->data << " ";
        return 1;//沿着路径返回1逆向把路径（祖先）输出
    }
    return 0;
}
int main()
{
    int flag;
    Bitree T;
    T=creatbitree();
    char x;
    cin>>x;
    if(T->data == x) cout<<"没有祖先结点";//根就是查找的结点，没有祖先
    else
    {
        flag = prtfa(T, x);
        if(flag==0)
            cout << x << "不存在";
    }
    return 0;
}