#include <iostream>
using namespace std;
#define maxsize 1000
typedef struct
{
    char data[maxsize];
    int top;
}Stack;

int brackets(char *str)//括号匹配
{
    int i, flag;
    flag = 1;
    Stack  fu;//符号栈
    fu.top = -1;
    i = 0;
    while(str[i]!='#')//表达式以#结尾
    {
        if(str[i]=='('||str[i]=='{'||str[i]=='[')//左括号入栈
        {
            fu.top++;
            fu.data[fu.top] = str[i];
            i++;
            continue;//立即进行下一次的循环
        }
        else//符号
        {
            if(str[i]==')')//遇到 )
            {
                if(fu.top==-1)//栈空但还有右括号
                {
                    flag = 0;
                    break;
                }
                else if(fu.data[fu.top] == '(')
                {
                    fu.top--;//出栈
                    flag = flag * 1;
                    i++;
                    continue;
                }
                else//不匹配
                {
                    flag = 0;
                    break;
                }
            }
           else if(str[i]==']')//遇到 ]
            {
                if(fu.top==-1)//栈空但还有右括号
                {
                    flag = 0;
                    break;
                }
                else if(fu.data[fu.top] == '[')
                {
                    fu.top--;//出栈
                    flag = flag * 1;
                    i++;
                    continue;
                }
                else//不匹配
                {
                    flag = 0;
                    break;
                }
            }
            else if(str[i]=='}')//遇到 }
            {
                if(fu.top==-1)//栈空但还有右括号
                {
                    flag = 0;
                    break;
                }
                else if(fu.data[fu.top] == '{')
                {
                    fu.top--;//出栈
                    flag = flag * 1;
                    i++;
                    continue;
                }
                else//不匹配
                {
                    flag = 0;
                    break;
                }
            }
            else
            {
                i++;
            }
        }
    }
    if(fu.top!=-1)//右括号匹配完但栈没有结束，有多余的左括号
    {
        flag = 0;
    }
    return flag;
}

int main()
{
    char str[maxsize];
    int a;//存标志符号
    gets(str);
    a = brackets(str);
    cout << a;
    return 0;
}