#include <iostream>
#include <stack>
#include <queue>
#define maxsize 1000
using namespace std;
typedef struct
{
    int key;//关键字
    int count;//比较次数
}Hash[maxsize];
void creatHash(Hash H, int m, int mod);
void cinHash(Hash H, int m, int mod, int k);

void cinHash(Hash H, int m,int mod, int k)
{
    int h;//散列地址
    int i, hi;
    h = k % mod; // 哈希函数H(k)=k%13
    if(H[h].key == -1)//不冲突，直接入表
    {
        H[h].key = k;//入散列表
        H[h].count = 1;//比较次数
    } 
    else
    {
        i = 0;
        do{
            h++;
            hi = h % m;//线性探测法解决冲突
            i++;//比较次数
        } while (H[hi].key != -1);
        H[hi].key = k;
        H[hi].count = i;
    }
}

void creatHash(Hash H,int m,int mod)
{
    int i, k;
    for (i = 0; i < m; i++)//初始化哈希表
    {
        H[i].key = -1;
        H[i].count = 0;
    }
    cin >> k;
    while (k != 0)
    {
        cinHash(H, m, mod, k);
        cin >> k;
    } 
}

int main()
{
    int i, m = 15, mod = 13; // 表长m=15,哈希函数mod13
    Hash H;
    creatHash(H, m, mod);
    for(i=0; i<m; i++)
        cout<<i<<" "<<H[i].key<<endl;
    return 0;
}