#include <iostream>
using namespace std;
typedef struct node
{
    int data;
    struct node *next;
}LNode;
void creat(LNode *p)//创链表
{ 
  LNode *q,*r=p;//q创结点，r串结点   尾插法
  int x;
  cin >> x;
  while(x!=0)
  {     
      //q=(LNode *)malloc(sizeof(LNode));
      q = new LNode;
      q->data = x;
      r->next = q;
      r = q;
      cin >> x;
  }
  r->next=NULL;
}
void output(LNode *H)//输出链表
{
    LNode *p;
    p=H->next;
    while(p!=NULL)
    {
        cout << p->data << ' ';
        //printf("%d  ",p->data);
        p=p->next;
    }
}
void link(LNode *HA,LNode *HB)//递增不重复连接链表A,B.尾插连接
{
    LNode *pre, *pa, *pb;
    pre = HA;
    pa = HA->next;
    pb = HB->next;
    while(pa!=NULL&&pb!=NULL)
    {
        if(pa->data<pb->data)
        {
            pre->next = pa;
            pre=pa;
            pa = pa->next;
        }
        else if(pa->data==pb->data)
        {
            pre->next = pa;
            pre=pa;
            pa = pa->next;
            pb = pb->next;
        }
        else
        {
            pre->next = pb;
            pre=pb;
            pb = pb->next;
        }
    }
    while(pa!=NULL)
    {
        pre->next = pa;
        pre = pa;
        pa = pa->next;
    }
    while(pb!=NULL)
    {
        pre->next = pb;
        pre = pb;
        pb = pb->next;
    }
    pre->next = NULL;
}
/*int long(LNode *H)//求链表长度
{
    LNode *p;
    int l=0;
    p = H->next;
    while(p!=NULL)
    {
        p = p->next;
        l++;
    }
    return l;
}*/

int main()
{
    LNode *HA,*HB;
    HA = new LNode;
    HA->next = NULL;
    HB = new LNode;
    HB->next = NULL;
    creat(HA);
    //output(HA);
    //cout << endl;
    creat(HB);
    //output(HB);
    //cout << endl;
    link(HA, HB);
    output(HA);
    return 0;
}
