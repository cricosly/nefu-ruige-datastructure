#include <iostream>
#include <queue>
#include <cstring>
using namespace std;
#define maxsize 1000
int lmax,i;
char road[1000],maxroad[1000];
typedef struct Binode
{
    char data;
    struct Binode *lchild, *rchild;
}BiNode,*Bitree;

Bitree creatbitree()//递归法先序建立二叉树,空指针@拓展
{
    Bitree T;
    char ch;
    //cin >> ch;
    scanf("%c", &ch);
    if(ch=='@')
        T = NULL;
    else
    {
        T = new BiNode;
        T->data = ch;
        T->lchild = creatbitree();
        T->rchild = creatbitree();
    }
    return T;
}

void longest(Bitree T,int l)
{
    if(T!=NULL)//递归结束条件
    {
        road[l] = T->data;//存路径
        if(T->lchild==NULL&&T->rchild==NULL)//叶子结点
        {
           if(l>lmax)//当前路径比原最长路径长，替换
           {
               lmax = l;
               for (i = 0; i <= lmax; i++)
               {
                   maxroad[i] = road[i];
               }
           }
        }
        else//非叶子结点
        {
            l++;
            longest(T->lchild,l);
            longest(T->rchild,l);
        }
    }
}

int main()
{
    Bitree T;//根
    int l = 0;
    lmax = 0;
    T=creatbitree();//递归法先序建立二叉树,空指针@拓展
    longest(T,l);
    cout << lmax+1 << ' ';//修正路径长度
    for (i = 0; i <= lmax; i++)
    {
        cout << maxroad[i] << ' ';
    }

        return 0;
}
