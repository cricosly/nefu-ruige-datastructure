#include <iostream>
#include <queue>
using namespace std;
#define maxsize 1000
typedef struct Binode
{
    char data;
    struct Binode *lchild, *rchild;
}BiNode,*Bitree;

Bitree creatbitree()//递归法先序建立二叉树,空指针@拓展
{
    Bitree T;
    char ch;
    //cin >> ch;
    scanf("%c", &ch);
    if(ch=='@')
        T = NULL;
    else
    {
        T = new BiNode;
        T->data = ch;
        T->lchild = creatbitree();
        T->rchild = creatbitree();
    }
    return T;
}

/*void output(Bitree T)//递归中序输出树
{
    if(T)
    {
        output(T->lchild);//先左孩子
        cout << T->data;//中序
        output(T->rchild);//后右孩子
    }
}*/

void lvsizetree(Bitree T)//层次遍历且输出每层宽度
{
    if(T==NULL)
        return;
    queue<Bitree> q;
    Bitree end = T, nextend = NULL, p;//end存当层最右边位置，nextend存下一层最右边位置
    int num;
    num = 0;
    q.push(T);//根入队
    cout << '1' << ' ';
    while(!q.empty())
    {
        p = q.front();//p==队首
        q.pop();//队首出队
        if(p->lchild!=NULL)//左孩子入队
        {
            q.push(p->lchild);
            nextend = p->lchild;
            num++;
        }
        if(p->rchild!=NULL)//右孩子入队
        {
            q.push(p->rchild);
            nextend = p->rchild;
            num++;
        }
        if(p==end&&!q.empty())
        {
            cout << num << ' ';
            end = nextend;
            num = 0;
        }
    }
}

int main()
{
    Bitree T;//根
    T=creatbitree();//递归法先序建立二叉树,空指针@拓展
    lvsizetree(T);//层次遍历且输出每层宽度
    return 0;
}
